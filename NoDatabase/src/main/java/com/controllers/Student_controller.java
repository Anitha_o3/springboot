package com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.model.Student;
import com.sevices.StudentService;

@RestController
@Component
public class Student_controller {
	@Autowired
	StudentService studentService;

	List<Student> list = new ArrayList<>();

	@GetMapping(value = "/students", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Student>> getStudents(@RequestParam(name = "age", required = false) Integer age) {
		List<Student> student = studentService.getAllStudents();
		return ResponseEntity.status(200).body(student);
	}

	@PostMapping(value = "/students", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		Student s = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(s);
	}

}
