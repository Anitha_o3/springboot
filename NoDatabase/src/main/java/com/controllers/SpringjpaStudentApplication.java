package com.controllers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class SpringjpaStudentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringjpaStudentApplication.class, args);
	}

}
