package com.attendence.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RestController;


@RestController
public class AttendenceController {
	@PostMapping(value="/attendences/{id}")
	@Retryable(value= {NumberFormatException.class},maxAttempts = 3)
	public ResponseEntity<String>findAttendence(@PathVariable("id") Integer id) {
		if(id==1)
		{
			return ResponseEntity.status(200).body("id found");
		}
		else
		{
				return ResponseEntity.status(200).body("id not found");
	    }
	}
	@Recover
	public ResponseEntity<String> recover(NumberFormatException ex)
	{
		return ResponseEntity.status(200).body("try again!!!!!! ");
	}
	
}
