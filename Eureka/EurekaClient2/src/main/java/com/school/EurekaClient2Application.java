package com.school;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@EnableEurekaClient
@SpringBootApplication
@RestController
public class EurekaClient2Application {	
	@Value("${server.port}")
	private int portNumber;

		public static void main(String[] args) {		
			SpringApplication.run(EurekaClient2Application.class, args);
		}

		@RequestMapping("/port")
		public ResponseEntity<Map> getMap() {
			Map map = new HashMap();
			map.put("port", portNumber);
			return ResponseEntity.status(200).body(map);
		}
				}

