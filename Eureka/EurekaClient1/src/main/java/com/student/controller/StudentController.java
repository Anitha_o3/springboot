package com.student.controller;

	import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

	@RestController
	public class StudentController {
		@Autowired
		RestTemplate restTemplate;
		@Value("${server.port}")
		private int portNumber;

		@RequestMapping("/test")
		public ResponseEntity<Map> getMap() {
			ResponseEntity<Map> response = restTemplate.getForEntity("http://School/port", Map.class);
			return response;
		}
	
	}


