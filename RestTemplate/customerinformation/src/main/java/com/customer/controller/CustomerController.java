package com.customer.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customer.module.Customer;

@RestController
public class CustomerController {
	@GetMapping(value = "customers")
	public List<Customer> getCustomer() {
		List<Customer> li = new ArrayList<Customer>();
		li.add(new Customer(1, "anitha", "anitha@gmail.com", 23));
		return li;

	}

}
