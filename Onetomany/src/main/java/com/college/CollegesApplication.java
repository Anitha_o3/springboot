package com.college;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EnableJpaRepositories
@SpringBootApplication
public class CollegesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollegesApplication.class, args);
	}

}
