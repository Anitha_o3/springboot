package com.college.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.model.College;
import com.college.model.Students;
import com.college.repository.College_repository;
import com.college.repository.Student_repository;


@Service
public class College_service {
	
		@Autowired
		private College_repository collegeRepository;
				
		public List<College> getAllCollege()
		{
			return collegeRepository.findAll();
		}
				public College saveCollege(College college)
		{
			return collegeRepository.save(college);
		}
		 
}


