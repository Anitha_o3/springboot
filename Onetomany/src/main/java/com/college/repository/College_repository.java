package com.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.college.model.College;
	@Repository
	public interface College_repository  extends JpaRepository<College, Integer>{

	}

