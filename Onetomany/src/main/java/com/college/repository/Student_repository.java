package com.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.college.model.Students;

@Repository
public interface Student_repository extends JpaRepository<Students, Integer> {

}
