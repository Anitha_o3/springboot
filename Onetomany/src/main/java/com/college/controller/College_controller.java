package com.college.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.college.model.College;
import com.college.service.College_service;

@RestController
@Component
public class College_controller {
		
	@Autowired
	private College_service  studService;
	@PostMapping(value = "/colleges")
	public ResponseEntity<College> createCollege(@RequestBody College college)	                                          {
		studService.saveCollege(college);
	    return ResponseEntity.status(201).body(college);
	}
	
	@GetMapping(value = "/colleges")
	public ResponseEntity<List<College> >getAllCollege(){
		List<College> college=studService.getAllCollege();
	    return ResponseEntity.status(201).body(college);
	}
}
