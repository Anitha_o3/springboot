package com.banking.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.banking.model.Bankdetails;
import com.banking.service.Bankservice;
@RestController
@Service
public class Bankcontroller {
	@Autowired
	private Bankservice bankservice;

	@GetMapping(value = "banks")
	public ResponseEntity<List<Bankdetails>> getDetails() {

		List<Bankdetails> bank = bankservice.findAlldetails();
		return ResponseEntity.status(200).body(bank);
	}
	@PostMapping(value="/banks")
	public ResponseEntity<Bankdetails> saveDetails(@Valid @RequestBody Bankdetails bank)
	{		
		return ResponseEntity.status(201).body(bankservice.saveAlldetails(bank));
	}
	@PutMapping(value="banks/{id}")
	public ResponseEntity<Bankdetails> updatedetails(@RequestBody Bankdetails bank,@PathVariable("id") int id)
	{
		bank.setId(id);
		return ResponseEntity.status(200).body(bankservice.updatedetails(bank, id));
	}
}

