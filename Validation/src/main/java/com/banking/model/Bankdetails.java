package com.banking.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.lang.NonNull;

@Entity
@Table(name="bank")
public class Bankdetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
private	int id;
private String name;
private int accountno;
@NonNull
@Size(max = 25)
@Suffix(message="ifsc code begin with Sbn letters")
private String ifsc;
public int getId(){
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAccountno() {
	return accountno;
}
public void setAccountno(int accountno) {
	this.accountno = accountno;
}
public String getIfsc() {
	return ifsc;
}
public void setIfsc(String ifsc) {
	this.ifsc = ifsc;
}

	public Bankdetails() {
	super();
}
	public Bankdetails(int id, String name, int accountno, String ifsc) {
		super();
		this.id = id;
		this.name = name;
		this.accountno = accountno;
		this.ifsc = ifsc;
	}

}
