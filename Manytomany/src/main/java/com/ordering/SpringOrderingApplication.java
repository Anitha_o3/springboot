package com.ordering;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOrderingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringOrderingApplication.class, args);
	}

}
