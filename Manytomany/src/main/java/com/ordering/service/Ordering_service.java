package com.ordering.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ordering.model.Member;
import com.ordering.model.Product;
import com.ordering.repository.Member_repository;
import com.ordering.repository.Product_repository;

@Service
public class Ordering_service {
	@Autowired
	private Member_repository memberRepository;

	@Autowired
	private Product_repository productRepository;

	public List<Member> getAllMembers() {
		return memberRepository.findAll();
	}

	public List<Product> getAllProducts() {
		return (List<Product>) productRepository.findAll();
	}

	public Member save(Member member) {
		return memberRepository.save(member);
	}

	public Product save(Product product) {
		return productRepository.save(product);
	}

}
