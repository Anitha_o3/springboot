package com.ordering.controller;

//import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ordering.model.Member;
//import com.ordering.model.Product;
import com.ordering.service.Ordering_service;

@RestController
@Component
public class Ordering_controller {
@Autowired
private Ordering_service order_product;

@GetMapping(value = "/members", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<?> getMember(){
	List<Member> member =order_product.getAllMembers();
	return ResponseEntity.status(200).body(member);
}


@PostMapping(value = "/members", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public ResponseEntity<?> saveMember(@RequestBody Member member) {
	Member p = order_product.save(member);
	return ResponseEntity.status(201).body(p);
}
}
