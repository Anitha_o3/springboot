package com.ordering.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ordering.model.Member;

@Repository
public interface Member_repository  extends JpaRepository<Member, Integer>{

}
