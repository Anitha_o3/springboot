package com.ordering.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ordering.model.Product;

@Repository
public interface Product_repository extends JpaRepository<Product, Integer> {

}
