package com.travel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.travel.model.Traveller;
import com.travel.repository.TravelRepository;

@Service
public class TravelService {
	@Autowired
	private TravelRepository travelrepository;
	
	public Traveller saveTraveller(Traveller travel) {
		return travelrepository.save(travel);
	}
	public List<Traveller> getTraveller() {
		return travelrepository.findAll();
	}
}
