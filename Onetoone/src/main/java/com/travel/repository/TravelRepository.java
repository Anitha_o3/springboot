package com.travel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.travel.model.Traveller;
//@Repository
public interface TravelRepository extends JpaRepository<Traveller, Integer> {

}
