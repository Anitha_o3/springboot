package com.travel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.travel.model.Traveller;
import com.travel.service.TravelService;

@Component
@RestController
public class TravelController {
	@Autowired
	private TravelService travelservice;
	@GetMapping(value = "/travellers", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Traveller>> getAll(){
		List<Traveller> travel =travelservice.getTraveller();
		return ResponseEntity.status(200).body(travel);
	}


	@PostMapping(value = "/travellers", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Traveller> saveAll(@RequestBody Traveller travel) {
		Traveller p = travelservice.saveTraveller(travel);
		return ResponseEntity.status(201).body(p);
	}
}
