package com.collectors.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.collectors.model.Student;
import com.collectors.model.Util;
import com.collectors.repository.StudentRepo;
@Service
public class StudentService {
	@Autowired
	private StudentRepo studentRepository;
	
	
	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

	public List<Student> getAllStudents() {
		return studentRepository.findAll();

	}
	public Student updateStudent(Integer id, Student student) {
		student.setId(id);
		return studentRepository.save(student);
	}
	public void deleteStudent(Integer id) {
	 studentRepository.deleteById(id);
	}
	public List<Student> findStudentByAge(int age){
		return studentRepository.findByAge(age);
	}
	public Student partialStudentUpdate(Integer id, Student student) {
		student.setId(id);
		Optional<Student> studentFromDb = studentRepository.findById(id);
		if (studentFromDb.isPresent()) {
			Student temp = studentFromDb.get();
			Util.myCopyProperties(student, temp);
			return studentRepository.save(temp);
		} else {
			return null;
		}
	}
	public Page<Student> findByPagination(Integer pageNumber, Integer noOfRecords) {

		return studentRepository.findAll(Util.pageObject(pageNumber, noOfRecords));
	}
	public long countOfAllStudents() {
		return studentRepository.count();
	}
	public List<Student> findStudentByIdAndAge(int id,int age){
		return studentRepository.findByIdAndAge(id,age);
	}
	public List<Student> findStudentByIdOrAge(int id,int age){
		return studentRepository.findByIdOrAge(id,age);
	}
	public List<Student> findStudentByIdBetween(int id,int id1){
		return studentRepository.findByIdBetween(id,id1);
	}
	public List<Student> findStudentByIdLessThan(int id){
		return studentRepository.findByIdLessThan(id);
	}
	public List<Student> findStudentByIdGreaterThan(int id){
		return studentRepository.findByIdGreaterThan(id);
	}
	public List<Student> findStudentByIdLessThanEqual(int id){
		return studentRepository.findByIdLessThanEqual(id);
	}
	public List<Student> findStudentByIdGreaterThanEqual(int id){
		return studentRepository.findByIdGreaterThanEqual(id);
	}
	public List<Student> findStudentByIdLike(int id){
		return studentRepository.findByIdLike(id);
	}
	public List<Student> findStudentByIdNotLike(int id){
		return studentRepository.findByIdNotLike(id);
	}
	public List<Student> findStudentByNameStartingWith(String name){
		return studentRepository.findByNameStartingWith(name);
	}
	public List<Student> findAllByStudentOrderByIdDesc(){
		return studentRepository.findAllByOrderByIdDesc();
	}
}
