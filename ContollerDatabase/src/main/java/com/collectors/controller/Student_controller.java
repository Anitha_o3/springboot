package com.collectors.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.collectors.model.Student;
import com.collectors.service.StudentService;

@RestController
@Component
public class Student_controller {
	@Autowired
	StudentService studentService;

	List<Student> list = new ArrayList<>();

	@GetMapping(value = "/students", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Student>> getStudents(@RequestParam(name = "age", required = false) Integer age){
		if(age!= null)
		{
			List<Student> student =studentService.findStudentByAge(age);
			return ResponseEntity.status(200).body(student);
		}else
		{
		List<Student> student =studentService.getAllStudents();
		return ResponseEntity.status(200).body(student);
	}
	}
	@PostMapping(value = "/students", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		Student s = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(s);
	}
	@PutMapping("students/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable("id") Integer id, @RequestBody Student student) {
		 student=studentService.updateStudent(id,student);
		return ResponseEntity.status(200).body(student);
	}

	@DeleteMapping("students/{id}")
	public void deleteStudent(@PathVariable("id") Integer id){
		studentService.deleteStudent(id);
	}
	@PatchMapping("students/{id}")
	public ResponseEntity<Student> partialUpdateStudent(@PathVariable("id") Integer id, @RequestBody Student student) {
		Student updatedStudent = studentService.partialStudentUpdate(id, student);
		return ResponseEntity.status(200).body(updatedStudent);
	}
	@GetMapping("/pagination")
	public ResponseEntity<List<Student>> findByPagination(Integer pageNumber, Integer noOfRecords){
	Page<Student> studentPage =studentService.findByPagination( pageNumber,  noOfRecords);
	//return Page.getContent(student);
	return ResponseEntity.status(200).body(studentPage.getContent());	
	}
@GetMapping ("/counts")
public long countOfAllStudents() {
		 return studentService.countOfAllStudents();		
}
@GetMapping(value = "/andstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdAndAge(@RequestParam("id")Integer id,@RequestParam("age")Integer age){
	if(id!=null && age!=null) {
		List<Student> student =studentService.findStudentByIdAndAge(id,age);
		return ResponseEntity.status(200).body(student);
	}else
	return ResponseEntity.status(404).build();
}
@GetMapping(value = "/orstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdOrAge(@RequestParam("id")Integer id,@RequestParam("age")Integer age){
if(id!=null && age!=null) {		
List<Student> student =studentService.findStudentByIdOrAge(id,age);
		return ResponseEntity.status(200).body(student);
}else
	return ResponseEntity.status(404).build();
}
@GetMapping(value = "/lessstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdLessThan(Integer id){
		List<Student> student =studentService.findStudentByIdLessThan(id);
		return ResponseEntity.status(200).body(student);
	
}
@GetMapping(value = "/greaterstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdGreaterThan(Integer id){
		List<Student> student =studentService.findStudentByIdGreaterThan(id);
		return ResponseEntity.status(200).body(student);	
}
@GetMapping(value = "/lesseqstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdLessThanEqual(Integer id){
		List<Student> student =studentService.findStudentByIdLessThanEqual(id);
		return ResponseEntity.status(200).body(student);	
}
@GetMapping(value = "/greatereqstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdGreaterThanEqual(Integer id){
		List<Student> student =studentService.findStudentByIdGreaterThanEqual(id);
		return ResponseEntity.status(200).body(student);	
}
@GetMapping(value = "/likestuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdLike(Integer id){
		List<Student> student =studentService.findStudentByIdLike(id);
		return ResponseEntity.status(200).body(student);	
}
@GetMapping(value = "/nlikestuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByIdNotLike(Integer id){
		List<Student> student =studentService.findStudentByIdNotLike(id);
		return ResponseEntity.status(200).body(student);	
}
@GetMapping(value = "/startsstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findByNameStartingWith(String name){
	List<Student> student =studentService.findStudentByNameStartingWith(name);
	return ResponseEntity.status(200).body(student);	
}
@GetMapping(value = "/descstuds", produces = { MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<List<Student>> findAllByOrderByIdDesc(){
	List<Student> student =studentService.findAllByStudentOrderByIdDesc();
	return ResponseEntity.status(200).body(student);	
}
}