package com.tickets.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tickets.model.Tickets;
import com.tickets.service.TicketsService;

@RestController
@Component
public class TicketsController {
	@Autowired
	private TicketsService ticketservice;

	@GetMapping(value = "details")
	public ResponseEntity<List<Tickets>> getDetails() {

		List<Tickets> ticket = ticketservice.getDetails();
		return ResponseEntity.status(200).body(ticket);
	}
	@PostMapping(value="details")
	public ResponseEntity<Tickets> saveDetails(@RequestBody Tickets ticket)
	{		
		return ResponseEntity.status(201).body(ticketservice.adddetails(ticket));
	}
	@PutMapping(value="details/{id}")
	public ResponseEntity<Tickets> updatedetails(@RequestBody Tickets tickets,@PathVariable("id") int id)
	{
		return ResponseEntity.status(200).body(ticketservice.updatedetails(tickets, id));
	}

}

