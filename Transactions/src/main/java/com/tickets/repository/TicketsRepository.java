package com.tickets.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tickets.model.Tickets;

@Repository
public interface TicketsRepository extends JpaRepository<Tickets, Integer>
{
	public Tickets findById(int id);

}
