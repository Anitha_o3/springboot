package com.tickets.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tickets")
public class Tickets {		
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE)
		int id;
		@Column
		String email;
		@Column
		String password;
		@Column
		String moviename;
		@Column
		int cardno;
		@Column
		int cost;
		public int getCardno() {
			return cardno;
		}
		public void setCardno(int cardno) {
			this.cardno = cardno;
		}
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getMoviename() {
			return moviename;
		}
		public void setMoviename(String moviename) {
			this.moviename = moviename;
		}
		public int getCost() {
			return cost;
		}
		public void setCost(int cost) {
			this.cost = cost;
		}

}
