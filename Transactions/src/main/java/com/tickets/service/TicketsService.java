package com.tickets.service;

import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;

import com.tickets.model.Tickets;
import com.tickets.repository.TicketsRepository;

@Service
public class TicketsService {
	@Autowired
	private TicketsRepository ticketsRepository;

	@Transactional//(isolation= Isolation.READ_COMMITTED)
	public Tickets adddetails(Tickets ticket) {

		return ticketsRepository.save(ticket);
	}

	@Transactional(rollbackOn = Exception.class)
	public Tickets updatedetails(Tickets ticket, int id) {
		Tickets ticket1 = ticketsRepository.saveAndFlush(ticket);
		try {

			int a = 10 / 0;
		} catch (Exception e) {

		}
		return ticket1;
	}

	public List<Tickets> getDetails() {
		List<Tickets> ticket = ticketsRepository.findAll();
		return ticket;
	}

}
