package com.async.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.async.model.Employee;

@Repository
public interface Employeerepository extends JpaRepository<Employee, Integer>{

}
