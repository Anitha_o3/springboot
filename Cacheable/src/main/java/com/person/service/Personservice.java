package com.person.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.person.model.Person;
import com.person.repository.PersonRepository;

@Service
public class Personservice {
	@Autowired
	private PersonRepository personrepo;
	public Person saveAllperson(Person person)
	{
		return personrepo.save(person);
	}
 @Cacheable(value="person")
	public List<Person> findAllperson(){
	return personrepo .findAll();
	}
@CachePut(value = "person",key="#person.id")
 public Person updateperson(Person person){
		return personrepo .saveAndFlush(person);
		}
	

}
