package com.person.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.person.model.Person;
import com.person.service.Personservice;



@RestController
@Component
public class Personcontroller {
	@Autowired
	private Personservice personservice;

	@GetMapping(value = "persons")
	public ResponseEntity<List<Person>> getDetails() {

		List<Person> person = personservice.findAllperson();
		return ResponseEntity.status(200).body(person);
	}
	@PostMapping(value="persons")
	public ResponseEntity<Person> saveDetails(@RequestBody Person person)
	{		
		return ResponseEntity.status(201).body(personservice.saveAllperson(person));
	}
	@PutMapping(value="persons/{id}")
	public ResponseEntity<Person> updatedetails(@RequestBody Person person,@PathVariable("id") int id)
	{
		person.setId(id);
		return ResponseEntity.status(200).body(personservice.updateperson(person));
	}
}
